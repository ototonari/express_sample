# ベースイメージの指定
FROM node:10.12

# 環境変数の設定
ENV NODE_ENV="development"

# 作業ディレクトリ作成＆設定
WORKDIR /app

COPY . /app

RUN npm install

EXPOSE 80

CMD ["npm", "start"]