const fs = require('fs')

function readTextFile (path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })
}

function writeTextFile (path, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (err) => {
      if (err) reject(err)
      resolve()
    })
  })
}

function getIndexPaths (reportPath) {
  return new Promise((resolve, reject) => {
    readTextFile('./public/reportList.txt').then(reports => {
      const indexPaths = reports.split('\n').filter(report => {
        const [, branch,] = report.split('/')
        return (branch)
      }).map(report => {
        const [, branch, developType, ...others] = report.split('/')
        const indexPath = [branch, developType, ...others].join('/')
        return indexPath
      }).sort((a, b) => a > b)
      console.log('paths: ', indexPaths)
      resolve(indexPaths)
    })
  })
}

function htmlTemplate(innerBody) {
  return `<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
    ${innerBody}
  </div>
  </body>
  </html>
  `
}

function makeTable ({ th, tds }) {
  const thTag = th.map(v => `<th scope="col">${v}</th>`).join('')
  const tr_tdTag = tds.map(td => {
    const [branch, paths] = td
    const tdTag = [`<td>${branch}</td>`].concat(paths.map(path => {
      const [, developType, ] = path.split('/')
      return `<td><a href="${path}">${developType}</a></td>`
    })).join('')

    return `<tr>${tdTag}</tr>`
  }).join('')
  const table = (th, tr_tdTag) => `
  <table class="table">
  <tr>${th}</tr> 
  ${tr_tdTag}
  </table>`
  return table(thTag, tr_tdTag)
}

function main() {
  const reportPath = './public/reportList.txt'
  getIndexPaths(reportPath).then(indexPaths => {
    const th = ['branch', 'front coverage', 'backend coverage']
    const m = new Map()
    indexPaths.forEach(path => {
      const [branch,] = path.split('/')
      if (m.has(branch)) {
        const values = m.get(branch)
        values.push(path)
        m.set(branch, values)
      } else {
        const value = [path]
        m.set(branch, value)
      }
    })
    // console.log(Array.from(m.entries()))
    const table = makeTable({ th, tds: Array.from(m.entries()) })

    const html = htmlTemplate(table)
    const indexHtmlPath = './public/index.html'
    writeTextFile(indexHtmlPath, html)
  })
}

main()