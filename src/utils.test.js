var utils = require('./utils');

test('adds 1 + 2 to equal 3', () => {
  expect(utils.sum(1, 2)).toBe(3);
});

test('subtraction 10 - 5 to equal 5', () => {
  expect(utils.subtraction(10, 5)).toBe(5);
});

test('multiplication 13 * 3 to equal 39', () => {
  expect(utils.multiplication(13, 3)).toBe(39);
});

test('divide 10 / 2 to equal 5', () => {
  expect(utils.divide(10, 2)).toBe(5);
});